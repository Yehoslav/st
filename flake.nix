{
  description = "A flake to build st.";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: {
      packages = let
        pkgs = import nixpkgs {inherit system;};
      in
        with pkgs; {
          default = stdenv.mkDerivation {
            name = "st";
            version = "0.8.5";
            src = ./.;

            nativeBuildInputs = [
              pkg-config
              ncurses
              fontconfig
              freetype
            ];

            buildInputs = [
              harfbuzz
              xorg.libX11
              xorg.libXft
            ];

            makeFlags = [
              "PKG_CONFIG=${stdenv.cc.targetPrefix}pkg-config"
            ];

            preInstall = ''
              export TERMINFO=$out/share/terminfo
            '';

            installFlags = [ "PREFIX=$(out)" ];

            installPhase = ''
              mkdir -p $out/bin
              cp -f st st-copyout st-urlhandler $out/bin
              chmod 755 $out/bin/st
              chmod 755 $out/bin/st-copyout
              chmod 755 $out/bin/st-urlhandler
            '';
          };
        };
    });
}
